﻿ARG CI
ARG BITBUCKET_DOCKER_HOST_INTERNAL
FROM mcr.microsoft.com/dotnet/runtime:5.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["docker-build-problem-example.csproj", "./"]
RUN dotnet restore "docker-build-problem-example.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "docker-build-problem-example.csproj" -c Release -o /app/build

FROM build AS test
ARG CI
ARG BITBUCKET_DOCKER_HOST_INTERNAL
RUN dotnet test "docker-build-problem-example.csproj" -c Release

FROM test AS publish
RUN dotnet publish "docker-build-problem-example.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "docker-build-problem-example.dll"]
