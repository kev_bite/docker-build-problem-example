using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Xunit;

namespace docker_build_problem_example
{
    public class UnitTest1
    {
        [Fact]
        public async Task Test1()
        {
            var client = new MongoClient($"mongodb://{GetHostName()}");
            var database = client.GetDatabase("test");
            var collection = database.GetCollection<BsonDocument>("docs");

            var items = await collection.Find(Builders<BsonDocument>.Filter.Empty)
                .ToListAsync();

            Assert.Empty(items);
        }

        private string GetHostName()
        {
            return Environment.GetEnvironmentVariable("CI") is { }
                ? Environment.GetEnvironmentVariable("BITBUCKET_DOCKER_HOST_INTERNAL") : "host.docker.internal";
        }
    }
}
